from django import forms
from .models import Statusgreet

class StatusgreetForm(forms.ModelForm):
    class Meta: 
        model = Statusgreet
        fields = [
            'status'
        ]
        widgets = {
            'status':forms.TextInput(attrs={"class":"form-control"}),
        }