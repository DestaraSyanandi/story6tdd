from django.db import models

class Statusgreet(models.Model):

    status = models.CharField(max_length=500)
    date   = models.DateTimeField(auto_now_add=True)
    
