from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from .models import Statusgreet
from . import forms

# Create your views here.

def utt(request):
    if request.method == "POST":
        form = forms.StatusgreetForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('testing:utt')

    else:
        form = forms.StatusgreetForm()

    return render (request, 'utt.html',{"form":form ,"list_statusgreet": Statusgreet.objects.all().order_by("-date")})