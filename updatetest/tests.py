from django.test import TestCase
from django.test import SimpleTestCase
from updatetest.forms import StatusgreetForm
from updatetest.models import Statusgreet
from django.test import Client
from django.urls import reverse
from updatetest.models import Statusgreet
from django.urls import reverse, resolve
from updatetest.views import utt
import json
from selenium import webdriver
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import reverse
from updatetest.models import Statusgreet
import time
from selenium.webdriver.chrome.options import Options
from . import forms



class TestFunctionalPage(StaticLiveServerTestCase):

    def setUp(self):

        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(TestFunctionalPage, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(TestFunctionalPage, self).tearDown()


    def test_element_in_project(self):
        self.browser.get(self.live_server_url)
        alert = self.browser.find_element_by_class_name("box")
        self.assertEquals(
            alert.find_element_by_tag_name('h5').text,
            'Hey! How are You (?)'


        )
        time.sleep(10)


    def test_response(self):
        project1 = Statusgreet.objects.create(
            status = "I'm fine"
        )
        self.browser.get(self.live_server_url)
        alert = self.browser.find_element_by_class_name("kotak")

        self.assertNotEquals(
            alert.find_element_by_tag_name('h5').text,
            "i'm fine"
        )
        time.sleep(10)
    
    def test_form(self):
        self.browser.get(self.live_server_url)

        time.sleep(5)
        form = self.browser.find_element_by_id('id_status')
        submit = self.browser.find_element_by_id('button')

        form.send_keys("Alhamdulillah baik...")
        submit.click()

        time.sleep(10)

        self.assertIn("Alhamdulillah baik...", self.browser.page_source)

       

class UnitTest(TestCase):

    def test_form_valid(self):
        form = StatusgreetForm(data= {
            'status':"Hai, bro"

        })
        self.assertTrue(form.is_valid())

    def test_no_data(self):
        form = StatusgreetForm(data={})

        self.assertFalse(form.is_valid())
        self.assertEquals(len(form.errors), 1)

    def setUp(self):
        self.project = Statusgreet.objects.create(
            status="Hai, bro"
        )
        self.project1 = Statusgreet.objects.create(
            status="Hai, bro"
        )

        self.client= Client()
        self.utt_url = reverse('testing:utt')

    def test_utt_GET(self):
        response = self.client.get(self.utt_url, {"form":'status'})
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'utt.html')

    def test_utt_POST_status(self):
        status_data = {
            'status':'halo'
        }
        form = forms.StatusgreetForm(data = status_data)
        self.assertTrue(form.is_valid())
        request = self.client.post(self.utt_url, data= status_data)
        self.assertEquals(request.status_code, 302)

        response = self.client.get(self.utt_url)
        self.assertEquals(response.status_code, 200)

    def test_project(self):
        self.assertNotEqual(self.project,self.project1)

    def test_utt_url_is_resolved(self):
        url = reverse('testing:utt')
        self.assertEquals(resolve(url).func , utt)

